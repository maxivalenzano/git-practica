import logo from './logo.svg';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Esto es una practica para Ing ST.
        </p>
        <a
          className="App-link"
          href="https://facebook.com/maxivalenzano"
          target="_blank"
          rel="noopener noreferrer"
        >
          Mi facebook
        </a>
      </header>
    </div>
  );
}

export default App;
