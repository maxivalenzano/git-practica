import { render, screen } from '@testing-library/react';
import App from './App';

test('test-link', () => {
  render(<App />);
  const texto = screen.getByText(/mi facebook/i);
  expect(texto).toBeInTheDocument();
});

test('test-title', () => {
  render(<App />);
  const texto = screen.getByText(/Esto es una practica para Ing SW./i);
  expect(texto).toBeInTheDocument();
});
